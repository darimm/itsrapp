﻿CREATE TABLE [dbo].[tbl_SystemsAnalysis]
(
	[SystemsAnalysisId] INT NOT NULL PRIMARY KEY IDENTITY,
	[DateUpdated] DATETIME NOT NULL,
	[DateCreated] DATETIME NOT NULL,
	[Notes] TEXT NOT NULL,
	[ResultID] SMALLINT NOT NULL,
	[StatusID] SMALLINT NOT NULL,
	[CustomerID] INT NOT NULL,
	[StandardID] INT NOT NULL,
	[CreatedBy] INT NOT NULL,
	[UpdatedBy] INT NOT NULL,
	[Active] BIT NOT NULL DEFAULT 1,
	CONSTRAINT FK_tbl_SystemsAnalysis_tbl_Standards FOREIGN KEY (StandardID)
	REFERENCES tbl_Standards ([StandardID])
	ON DELETE CASCADE
	ON UPDATE CASCADE,
	CONSTRAINT FK_tbl_SystemsAnalysis_tbl_Customers FOREIGN KEY (CustomerID)
	REFERENCES tbl_Customers ([CustomerID])
	ON DELETE CASCADE
	ON UPDATE CASCADE,
	CONSTRAINT FK_tbl_SystemsAnalysis_tbl_ReviewResult FOREIGN KEY (ResultID)
	REFERENCES tbl_ReviewResult ([ResultID])
	ON DELETE CASCADE
	ON UPDATE CASCADE,
	CONSTRAINT FK_tbl_SystemsAnalysis_tbl_Status FOREIGN KEY (StatusID)
	REFERENCES tbl_Status (StatusID)
	ON DELETE CASCADE
	ON UPDATE CASCADE,
	CONSTRAINT FK_tbl_SystemsAnalysis_tbl_Users1 FOREIGN KEY (UpdatedBy)
	REFERENCES tbl_Users ([UserID])
	ON DELETE NO ACTION
	ON UPDATE NO ACTION,
	CONSTRAINT FK_tbl_SystemsAnalysis_tbl_Users2 FOREIGN KEY (CreatedBy)
	REFERENCES tbl_Users ([UserID])
	ON DELETE NO ACTION
	ON UPDATE NO ACTION
)
