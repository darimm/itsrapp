﻿CREATE TABLE [dbo].[tbl_Category]
(
	[CategoryID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Category] VARCHAR(50) NOT NULL,
	[Active] BIT NOT NULL DEFAULT 1
)

