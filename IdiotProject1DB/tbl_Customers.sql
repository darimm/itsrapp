﻿CREATE TABLE [dbo].[tbl_Customers]
(
	[CustomerID] INT NOT NULL PRIMARY KEY IDENTITY,
	[CustomerName] TEXT NOT NULL,
	[CustomerBUSINESSID] INT NOT NULL,
	[CustomerMAP] BIT NOT NULL,
	[Active] BIT NOT NULL DEFAULT 1
)
