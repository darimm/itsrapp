﻿CREATE TABLE [dbo].[tbl_Standards]
(
	[StandardID] INT NOT NULL PRIMARY KEY IDENTITY,
	[Item] TEXT NOT NULL,
	[Description] TEXT NOT NULL,
	[Priority] SMALLINT NOT NULL,
	[WhenID] SMALLINT NOT NULL,
	[CategoryID] INT NOT NULL,
	[SubcategoryID] INT NOT NULL,
	[Active] BIT NOT NULL DEFAULT 1,
	CONSTRAINT FK_tbl_Standards_tbl_Category FOREIGN KEY (CategoryID) 
	REFERENCES tbl_Category (CategoryID)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION,
	CONSTRAINT FK_tbl_Stardards_tbl_Subcatgeory FOREIGN KEY (SubcategoryID)
	REFERENCES tbl_SubCategory ([SubcategoryID])
	ON DELETE NO ACTION
	ON UPDATE NO ACTION,
	CONSTRAINT FK_tbl_Stardards_tbl_StandardsWhen FOREIGN KEY (WhenID)
	REFERENCES tbl_StandardsWhen ([WhenID])
	ON DELETE NO ACTION
	ON UPDATE NO ACTION
	)
