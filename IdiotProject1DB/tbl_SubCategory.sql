﻿CREATE TABLE [dbo].[tbl_SubCategory]
(
	[SubcategoryID] INT NOT NULL PRIMARY KEY IDENTITY,
	[Subcategory] VARCHAR(50) NOT NULL,
	[CategoryID] INT NOT NULL,
	[Active] BIT NOT NULL DEFAULT 1,
	CONSTRAINT FK_tbl_SubCategory_tbl_Category FOREIGN KEY (CategoryID)
	REFERENCES tbl_Category (CategoryID)
	ON DELETE CASCADE
	ON UPDATE CASCADE
)
