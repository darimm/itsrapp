﻿CREATE TABLE [dbo].[tbl_Status]
(
	[StatusID] SMALLINT NOT NULL PRIMARY KEY,
	[Status] varchar(50) NOT NULL,
	[Active] BIT NOT NULL DEFAULT 1
)
