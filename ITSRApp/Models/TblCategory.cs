﻿using System;
using System.Collections.Generic;

namespace ITSRApp.Models
{
    public partial class TblCategory
    {
        public TblCategory()
        {
            TblStandards = new HashSet<TblStandards>();
            TblSubCategory = new HashSet<TblSubCategory>();
        }

        public int CategoryId { get; set; }
        public string Category { get; set; }
        public bool? Active { get; set; }

        public ICollection<TblStandards> TblStandards { get; set; }
        public ICollection<TblSubCategory> TblSubCategory { get; set; }
    }
}
