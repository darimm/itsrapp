﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ITSRApp.Models
{
    public partial class ITSRDatabaseContext : DbContext
    {
        public ITSRDatabaseContext()
        {
        }

        public ITSRDatabaseContext(DbContextOptions<ITSRDatabaseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TblCategory> TblCategory { get; set; }
        public virtual DbSet<TblCustomers> TblCustomers { get; set; }
        public virtual DbSet<TblReviewResult> TblReviewResult { get; set; }
        public virtual DbSet<TblStandards> TblStandards { get; set; }
        public virtual DbSet<TblStandardsWhen> TblStandardsWhen { get; set; }
        public virtual DbSet<TblStatus> TblStatus { get; set; }
        public virtual DbSet<TblSubCategory> TblSubCategory { get; set; }
        public virtual DbSet<TblSystemsAnalysis> TblSystemsAnalysis { get; set; }
        public virtual DbSet<TblUsers> TblUsers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=(localdb)\\ProjectsV13;Initial Catalog=ITSRDatabase;Integrated Security=True;Connect Timeout=30;ApplicationIntent=ReadWrite");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TblCategory>(entity =>
            {
                entity.HasKey(e => e.CategoryId);

                entity.ToTable("tbl_Category");

                entity.Property(e => e.CategoryId).HasColumnName("CategoryID");

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Category)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblCustomers>(entity =>
            {
                entity.HasKey(e => e.CustomerId);

                entity.ToTable("tbl_Customers");

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.CustomerBusinessid).HasColumnName("CustomerBUSINESSID");

                entity.Property(e => e.CustomerMap).HasColumnName("CustomerMAP");

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasColumnType("text");
            });

            modelBuilder.Entity<TblReviewResult>(entity =>
            {
                entity.HasKey(e => e.ResultId);

                entity.ToTable("tbl_ReviewResult");

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ResultText)
                    .IsRequired()
                    .HasColumnType("text");
            });

            modelBuilder.Entity<TblStandards>(entity =>
            {
                entity.HasKey(e => e.StandardId);

                entity.ToTable("tbl_Standards");

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.CategoryId).HasColumnName("CategoryID");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnType("text");

                entity.Property(e => e.Item)
                    .IsRequired()
                    .HasColumnType("text");

                entity.Property(e => e.SubcategoryId).HasColumnName("SubcategoryID");

                entity.Property(e => e.WhenId).HasColumnName("WhenID");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.TblStandards)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tbl_Standards_tbl_Category");

                entity.HasOne(d => d.Subcategory)
                    .WithMany(p => p.TblStandards)
                    .HasForeignKey(d => d.SubcategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tbl_Stardards_tbl_Subcatgeory");

                entity.HasOne(d => d.When)
                    .WithMany(p => p.TblStandards)
                    .HasForeignKey(d => d.WhenId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tbl_Stardards_tbl_StandardsWhen");
            });

            modelBuilder.Entity<TblStandardsWhen>(entity =>
            {
                entity.HasKey(e => e.WhenId);

                entity.ToTable("tbl_StandardsWhen");

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Step)
                    .IsRequired()
                    .HasColumnType("text");
            });

            modelBuilder.Entity<TblStatus>(entity =>
            {
                entity.HasKey(e => e.StatusId);

                entity.ToTable("tbl_Status");

                entity.Property(e => e.StatusId)
                    .HasColumnName("StatusID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblSubCategory>(entity =>
            {
                entity.HasKey(e => e.SubcategoryId);

                entity.ToTable("tbl_SubCategory");

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.CategoryId).HasColumnName("CategoryID");

                entity.Property(e => e.Subcategory)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.TblSubCategory)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_tbl_SubCategory_tbl_Category");
            });

            modelBuilder.Entity<TblSystemsAnalysis>(entity =>
            {
                entity.HasKey(e => e.SystemsAnalysisId);

                entity.ToTable("tbl_SystemsAnalysis");

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.DateUpdated).HasColumnType("datetime");

                entity.Property(e => e.Notes)
                    .IsRequired()
                    .HasColumnType("text");

                entity.Property(e => e.ResultId).HasColumnName("ResultID");

                entity.Property(e => e.StandardId).HasColumnName("StandardID");

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.TblSystemsAnalysisCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tbl_SystemsAnalysis_tbl_Users2");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.TblSystemsAnalysis)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK_tbl_SystemsAnalysis_tbl_Customers");

                entity.HasOne(d => d.Result)
                    .WithMany(p => p.TblSystemsAnalysis)
                    .HasForeignKey(d => d.ResultId)
                    .HasConstraintName("FK_tbl_SystemsAnalysis_tbl_ReviewResult");

                entity.HasOne(d => d.Standard)
                    .WithMany(p => p.TblSystemsAnalysis)
                    .HasForeignKey(d => d.StandardId)
                    .HasConstraintName("FK_tbl_SystemsAnalysis_tbl_Standards");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.TblSystemsAnalysis)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_tbl_SystemsAnalysis_tbl_Status");

                entity.HasOne(d => d.UpdatedByNavigation)
                    .WithMany(p => p.TblSystemsAnalysisUpdatedByNavigation)
                    .HasForeignKey(d => d.UpdatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tbl_SystemsAnalysis_tbl_Users1");
            });

            modelBuilder.Entity<TblUsers>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("tbl_Users");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.OAuth)
                    .HasColumnName("oAuth")
                    .HasColumnType("text");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasColumnType("text");
            });
        }
    }
}
