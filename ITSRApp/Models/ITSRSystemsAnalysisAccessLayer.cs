﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ITSRApp.Models
{
    
    public class ITSRSystemsAnalysisAccessLayer
    {
        ITSRDatabaseContext db = new ITSRDatabaseContext();

        //Return a list of all Standards systemsanalysis entries
        public IEnumerable<TblSystemsAnalysis> GetAll()
        {
            try
            {
                return db.TblSystemsAnalysis.ToList();
            }
            catch
            {
                throw;
            }
        }

        //Add a Standards systemsanalysis entry
        public int Add(TblSystemsAnalysis systemsanalysis)
        {
            try
            {
                db.TblSystemsAnalysis.Add(systemsanalysis);
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        //Update a Standards systemsanalysis entry
        public int Update(TblSystemsAnalysis systemsanalysis)
        {
            try
            {
                db.Entry(systemsanalysis).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        //Retrieve a single Stardards systemsanalysis entry
        public TblSystemsAnalysis Get(int id)
        {
            try
            {
                TblSystemsAnalysis systemsanalysis = db.TblSystemsAnalysis.Find(id);
                return systemsanalysis;
            }
            catch
            {
                throw;
            }
        }

    }
}
