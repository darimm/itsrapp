﻿using System;
using System.Collections.Generic;

namespace ITSRApp.Models
{
    public partial class TblCustomers
    {
        public TblCustomers()
        {
            TblSystemsAnalysis = new HashSet<TblSystemsAnalysis>();
        }

        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int CustomerBusinessid { get; set; }
        public bool CustomerMap { get; set; }
        public bool? Active { get; set; }

        public ICollection<TblSystemsAnalysis> TblSystemsAnalysis { get; set; }
    }
}
