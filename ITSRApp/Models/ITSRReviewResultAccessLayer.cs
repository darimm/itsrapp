﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ITSRApp.Models
{
    
    public class ITSRReviewResultAccessLayer
    {
        ITSRDatabaseContext db = new ITSRDatabaseContext();

        //Return a list of all Standards Review Result entries
        public IEnumerable<TblReviewResult> GetAll()
        {
            try
            {
                return db.TblReviewResult.ToList();
            }
            catch
            {
                throw;
            }
        }

        //Add a Standards Review Result entry
        public int Add(TblReviewResult reviewresult)
        {
            try
            {
                db.TblReviewResult.Add(reviewresult);
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        //Update a Standards Review Result entry
        public int Update(TblReviewResult reviewresult)
        {
            try
            {
                db.Entry(reviewresult).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        //Retrieve a single Stardards reviewresult entry
        public TblReviewResult Get(int id)
        {
            try
            {
                TblReviewResult reviewresult = db.TblReviewResult.Find(id);
                return reviewresult;
            }
            catch
            {
                throw;
            }
        }

    }
}
