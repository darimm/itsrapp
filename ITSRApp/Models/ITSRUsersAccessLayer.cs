﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ITSRApp.Models
{
    
    public class ITSRUsersAccessLayer
    {
        ITSRDatabaseContext db = new ITSRDatabaseContext();

        //Return a list of all Standards user entries
        public IEnumerable<TblUsers> GetAll()
        {
            try
            {
                return db.TblUsers.ToList();
            }
            catch
            {
                throw;
            }
        }

        //Add a Standards user entry
        public int Add(TblUsers user)
        {
            try
            {
                db.TblUsers.Add(user);
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        //Update a Standards user entry
        public int Update(TblUsers user)
        {
            try
            {
                db.Entry(user).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        //Retrieve a single Stardards user entry
        public TblUsers Get(int id)
        {
            try
            {
                TblUsers user = db.TblUsers.Find(id);
                return user;
            }
            catch
            {
                throw;
            }
        }

    }
}
