﻿using System;
using System.Collections.Generic;

namespace ITSRApp.Models
{
    public partial class TblStandardsWhen
    {
        public TblStandardsWhen()
        {
            TblStandards = new HashSet<TblStandards>();
        }

        public short WhenId { get; set; }
        public string Step { get; set; }
        public bool? Active { get; set; }

        public ICollection<TblStandards> TblStandards { get; set; }
    }
}
