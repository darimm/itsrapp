﻿using System;
using System.Collections.Generic;

namespace ITSRApp.Models
{
    public partial class TblReviewResult
    {
        public TblReviewResult()
        {
            TblSystemsAnalysis = new HashSet<TblSystemsAnalysis>();
        }

        public short ResultId { get; set; }
        public string ResultText { get; set; }
        public bool? Active { get; set; }

        public ICollection<TblSystemsAnalysis> TblSystemsAnalysis { get; set; }
    }
}
