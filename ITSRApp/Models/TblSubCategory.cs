﻿using System;
using System.Collections.Generic;

namespace ITSRApp.Models
{
    public partial class TblSubCategory
    {
        public TblSubCategory()
        {
            TblStandards = new HashSet<TblStandards>();
        }

        public int SubcategoryId { get; set; }
        public string Subcategory { get; set; }
        public int CategoryId { get; set; }
        public bool? Active { get; set; }

        public TblCategory Category { get; set; }
        public ICollection<TblStandards> TblStandards { get; set; }
    }
}
