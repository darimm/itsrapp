﻿using System;
using System.Collections.Generic;

namespace ITSRApp.Models
{
    public partial class TblStatus
    {
        public TblStatus()
        {
            TblSystemsAnalysis = new HashSet<TblSystemsAnalysis>();
        }

        public short StatusId { get; set; }
        public string Status { get; set; }
        public bool? Active { get; set; }

        public ICollection<TblSystemsAnalysis> TblSystemsAnalysis { get; set; }
    }
}
