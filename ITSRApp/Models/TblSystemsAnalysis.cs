﻿using System;
using System.Collections.Generic;

namespace ITSRApp.Models
{
    public partial class TblSystemsAnalysis
    {
        public int SystemsAnalysisId { get; set; }
        public DateTime DateUpdated { get; set; }
        public DateTime DateCreated { get; set; }
        public string Notes { get; set; }
        public short ResultId { get; set; }
        public short StatusId { get; set; }
        public int CustomerId { get; set; }
        public int StandardId { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public bool? Active { get; set; }

        public TblUsers CreatedByNavigation { get; set; }
        public TblCustomers Customer { get; set; }
        public TblReviewResult Result { get; set; }
        public TblStandards Standard { get; set; }
        public TblStatus Status { get; set; }
        public TblUsers UpdatedByNavigation { get; set; }
    }
}
