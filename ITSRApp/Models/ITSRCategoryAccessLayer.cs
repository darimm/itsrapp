﻿using System.Collections.Generic;
using System.Linq;

namespace ITSRApp.Models
{

    public class ITSRCategoryAccessLayer
    {
        ITSRDatabaseContext db = new ITSRDatabaseContext();
        
        //Return a list of all Standards Category entries
        public IEnumerable<TblCategory> GetAll()
        {
            try
            {
                return db.TblCategory.ToList();
            }
            catch
            {
                throw;
            }
        }

        //Add a Standards Category entry
        public int Add(TblCategory category)
        {
            try
            {
                db.TblCategory.Add(category);
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        //Update a Standards Category entry
        public int Update(TblCategory category)
        {
            try
            {
                db.Entry(category).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        //Retrieve a single Stardards Category entry
        public TblCategory Get(int id)
        {
            try
            {
                TblCategory category = db.TblCategory.Find(id);
                return category;
            }
            catch
            {
                throw;
            }
        }

    }
}
