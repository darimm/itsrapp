﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ITSRApp.Models
{
    
    public class ITSRStatusAccessLayer
    {
        ITSRDatabaseContext db = new ITSRDatabaseContext();

        //Return a list of all Standards status entries
        public IEnumerable<TblStatus> GetAll()
        {
            try
            {
                return db.TblStatus.ToList();
            }
            catch
            {
                throw;
            }
        }

        //Add a Standards status entry
        public int Add(TblStatus status)
        {
            try
            {
                db.TblStatus.Add(status);
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        //Update a Standards status entry
        public int Update(TblStatus status)
        {
            try
            {
                db.Entry(status).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        //Retrieve a single Stardards status entry
        public TblStatus Get(int id)
        {
            try
            {
                TblStatus status = db.TblStatus.Find(id);
                return status;
            }
            catch
            {
                throw;
            }
        }

    }
}
