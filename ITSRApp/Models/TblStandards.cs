﻿using System;
using System.Collections.Generic;

namespace ITSRApp.Models
{
    public partial class TblStandards
    {
        public TblStandards()
        {
            TblSystemsAnalysis = new HashSet<TblSystemsAnalysis>();
        }

        public int StandardId { get; set; }
        public string Item { get; set; }
        public string Description { get; set; }
        public short Priority { get; set; }
        public short WhenId { get; set; }
        public int CategoryId { get; set; }
        public int SubcategoryId { get; set; }
        public bool? Active { get; set; }

        public TblCategory Category { get; set; }
        public TblSubCategory Subcategory { get; set; }
        public TblStandardsWhen When { get; set; }
        public ICollection<TblSystemsAnalysis> TblSystemsAnalysis { get; set; }
    }
}
