﻿using System;
using System.Collections.Generic;

namespace ITSRApp.Models
{
    public partial class TblUsers
    {
        public TblUsers()
        {
            TblSystemsAnalysisCreatedByNavigation = new HashSet<TblSystemsAnalysis>();
            TblSystemsAnalysisUpdatedByNavigation = new HashSet<TblSystemsAnalysis>();
        }

        public int UserId { get; set; }
        public string UserName { get; set; }
        public string OAuth { get; set; }
        public bool Admin { get; set; }
        public bool? Active { get; set; }

        public ICollection<TblSystemsAnalysis> TblSystemsAnalysisCreatedByNavigation { get; set; }
        public ICollection<TblSystemsAnalysis> TblSystemsAnalysisUpdatedByNavigation { get; set; }
    }
}
