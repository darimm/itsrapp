﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ITSRApp.Models
{
    
    public class ITSRStandardsAccessLayer
    {
        ITSRDatabaseContext db = new ITSRDatabaseContext();

        //Return a list of all Standards Standard entries
        public IEnumerable<TblStandards> GetAll()
        {
            try
            {
                return db.TblStandards.ToList();
            }
            catch
            {
                throw;
            }
        }

        //Add a Standards Standard entry
        public int Add(TblStandards standards)
        {
            try
            {
                db.TblStandards.Add(standards);
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        //Update a Standards Standard entry
        public int Update(TblStandards standards)
        {
            try
            {
                db.Entry(standards).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        //Retrieve a single Stardards Standard entry
        public TblStandards Get(int id)
        {
            try
            {
                TblStandards Standards = db.TblStandards.Find(id);
                return Standards;
            }
            catch
            {
                throw;
            }
        }

    }
}
