﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ITSRApp.Models
{
    
    public class ITSRCustomerAccessLayer
    {
        ITSRDatabaseContext db = new ITSRDatabaseContext();

        //Return a list of all Standards Customer entries
        public IEnumerable<TblCustomers> GetAll()
        {
            try
            {
                return db.TblCustomers.ToList();
            }
            catch
            {
                throw;
            }
        }

        //Add a Standards Customer entry
        public int Add(TblCustomers customer)
        {
            try
            {
                db.TblCustomers.Add(customer);
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        //Update a Standards Customer entry
        public int Update(TblCustomers customer)
        {
            try
            {
                db.Entry(customer).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        //Retrieve a single Stardards customer entry
        public TblCustomers Get(int id)
        {
            try
            {
                TblCustomers customer = db.TblCustomers.Find(id);
                return customer;
            }
            catch
            {
                throw;
            }
        }

    }
}
