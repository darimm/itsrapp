﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ITSRApp.Models
{
    
    public class ITSRWhenAccessLayer
    {
        ITSRDatabaseContext db = new ITSRDatabaseContext();

        //Return a list of all Standards when entries
        public IEnumerable<TblStandardsWhen> GetAll()
        {
            try
            {
                return db.TblStandardsWhen.ToList();
            }
            catch
            {
                throw;
            }
        }

        //Add a Standards when entry
        public int Add(TblStandardsWhen when)
        {
            try
            {
                db.TblStandardsWhen.Add(when);
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        //Update a Standards when entry
        public int Update(TblStandardsWhen when)
        {
            try
            {
                db.Entry(when).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        //Retrieve a single Stardards when entry
        public TblStandardsWhen Get(int id)
        {
            try
            {
                TblStandardsWhen when = db.TblStandardsWhen.Find(id);
                return when;
            }
            catch
            {
                throw;
            }
        }

    }
}
