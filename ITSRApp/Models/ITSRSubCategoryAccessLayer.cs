﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ITSRApp.Models
{
    
    public class ITSRSubCategoryAccessLayer
    {
        ITSRDatabaseContext db = new ITSRDatabaseContext();

        //Return a list of all Standards subcategory entries
        public IEnumerable<TblSubCategory> GetAll()
        {
            try
            {
                return db.TblSubCategory.ToList();
            }
            catch
            {
                throw;
            }
        }

        //Add a Standards subcategory entry
        public int Add(TblSubCategory subcategory)
        {
            try
            {
                db.TblSubCategory.Add(subcategory);
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        //Update a Standards subcategory entry
        public int Update(TblSubCategory subcategory)
        {
            try
            {
                db.Entry(subcategory).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        //Retrieve a single Stardards subcategory entry
        public TblSubCategory Get(int id)
        {
            try
            {
                TblSubCategory subcategory = db.TblSubCategory.Find(id);
                return subcategory;
            }
            catch
            {
                throw;
            }
        }

    }
}
