using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ITSRApp.Models;

namespace ITSRApp.Controllers
{
    public class ITSRStandardsController : Controller
    {
        ITSRStandardsAccessLayer oStandard = new ITSRStandardsAccessLayer();

        [HttpGet]
        [Route("api/Standards/Index")]
        public IEnumerable<TblStandards> Index()
        {
            return oStandard.GetAll();
        }

        [HttpPost]
        [Route("api/Standards/Create")]
        public int Create([FromBody] TblStandards standard)
        {
            return oStandard.Add(standard);
        }

        [HttpGet]
        [Route("api/Standards/Details/{id}")]
        public TblStandards Details(int id)
        {
            return oStandard.Get(id);
        }

        [HttpPut]
        [Route("api/Standards/Edit")]
        public int Edit([FromBody]TblStandards standard)
        {
            return oStandard.Update(standard);
        }
    }
}
