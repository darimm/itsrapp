using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ITSRApp.Models;

namespace ITSRApp.Controllers
{
    public class ITSRCategoryController : Controller
    {
        ITSRCategoryAccessLayer oCategory = new ITSRCategoryAccessLayer();

        [HttpGet]
        [Route("api/Category/Index")]
        public IEnumerable<TblCategory> Index()
        {
            return oCategory.GetAll();
        }

        [HttpPost]
        [Route("api/Category/Create")]
        public int Create([FromBody] TblCategory category)
        {
            return oCategory.Add(category);
        }

        [HttpGet]
        [Route("api/Category/Details/{id}")]
        public TblCategory Details(int id)
        {
            return oCategory.Get(id);
        }

        [HttpPut]
        [Route("api/Category/Edit")]
        public int Edit([FromBody]TblCategory category)
        {
            return oCategory.Update(category);
        }
    }
}
