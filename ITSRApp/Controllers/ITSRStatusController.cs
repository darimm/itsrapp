using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ITSRApp.Models;

namespace ITSRApp.Controllers
{
    public class ITSRStatusController : Controller
    {
        ITSRStatusAccessLayer oStatus = new ITSRStatusAccessLayer();

        [HttpGet]
        [Route("api/Status/Index")]
        public IEnumerable<TblStatus> Index()
        {
            return oStatus.GetAll();
        }

        [HttpPost]
        [Route("api/Status/Create")]
        public int Create([FromBody] TblStatus status)
        {
            return oStatus.Add(status);
        }

        [HttpGet]
        [Route("api/Status/Details/{id}")]
        public TblStatus Details(int id)
        {
            return oStatus.Get(id);
        }

        [HttpPut]
        [Route("api/Status/Edit")]
        public int Edit([FromBody]TblStatus status)
        {
            return oStatus.Update(status);
        }
    }
}
