using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ITSRApp.Models;

namespace ITSRApp.Controllers
{
    public class ITSRSubCategoryController : Controller
    {
        ITSRSubCategoryAccessLayer oSubCategory = new ITSRSubCategoryAccessLayer();

        [HttpGet]
        [Route("api/SubCategory/Index")]
        public IEnumerable<TblSubCategory> Index()
        {
            return oSubCategory.GetAll();
        }

        [HttpPost]
        [Route("api/SubCategory/Create")]
        public int Create([FromBody] TblSubCategory subcategory)
        {
            return oSubCategory.Add(subcategory);
        }

        [HttpGet]
        [Route("api/SubCategory/Details/{id}")]
        public TblSubCategory Details(int id)
        {
            return oSubCategory.Get(id);
        }

        [HttpPut]
        [Route("api/SubCategory/Edit")]
        public int Edit([FromBody]TblSubCategory subcategory)
        {
            return oSubCategory.Update(subcategory);
        }
    }
}
