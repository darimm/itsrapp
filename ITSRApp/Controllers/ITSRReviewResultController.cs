using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ITSRApp.Models;

namespace ITSRApp.Controllers
{
    public class ITSRReviewResultController : Controller
    {
        ITSRReviewResultAccessLayer oReviewResult = new ITSRReviewResultAccessLayer();

        [HttpGet]
        [Route("api/ReviewResult/Index")]
        public IEnumerable<TblReviewResult> Index()
        {
            return oReviewResult.GetAll();
        }

        [HttpPost]
        [Route("api/ReviewResult/Create")]
        public int Create([FromBody] TblReviewResult reviewresult)
        {
            return oReviewResult.Add(reviewresult);
        }

        [HttpGet]
        [Route("api/ReviewResult/Details/{id}")]
        public TblReviewResult Details(int id)
        {
            return oReviewResult.Get(id);
        }

        [HttpPut]
        [Route("api/ReviewResult/Edit")]
        public int Edit([FromBody]TblReviewResult reviewresult)
        {
            return oReviewResult.Update(reviewresult);
        }
    }
}
