using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ITSRApp.Models;

namespace ITSRApp.Controllers
{
    public class ITSRSystemsAnalysisController : Controller
    {
        ITSRSystemsAnalysisAccessLayer oSystemsAnalysis = new ITSRSystemsAnalysisAccessLayer();

        [HttpGet]
        [Route("api/SystemsAnalysis/Index")]
        public IEnumerable<TblSystemsAnalysis> Index()
        {
            return oSystemsAnalysis.GetAll();
        }

        [HttpPost]
        [Route("api/SystemsAnalysis/Create")]
        public int Create([FromBody] TblSystemsAnalysis analysis)
        {
            return oSystemsAnalysis.Add(analysis);
        }

        [HttpGet]
        [Route("api/SystemsAnalysis/Details/{id}")]
        public TblSystemsAnalysis Details(int id)
        {
            return oSystemsAnalysis.Get(id);
        }

        [HttpPut]
        [Route("api/SystemsAnalysis/Edit")]
        public int Edit([FromBody]TblSystemsAnalysis analysis)
        {
            return oSystemsAnalysis.Update(analysis);
        }
    }
}
