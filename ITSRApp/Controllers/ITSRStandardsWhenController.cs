using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ITSRApp.Models; 

namespace ITSRApp.Controllers
{
   public class ITSRStandardsWhenController : Controller
    {
        ITSRWhenAccessLayer oWhen = new ITSRWhenAccessLayer();

        [HttpGet]
        [Route("api/StandardsWhen/Index")]
        public IEnumerable<TblStandardsWhen> Index()
        {
            return oWhen.GetAll();
        }

        [HttpPost]
        [Route("api/StandardsWhen/Create")]
        public int Create([FromBody] TblStandardsWhen standardswhen)
        {
            return oWhen.Add(standardswhen);
        }

        [HttpGet]
        [Route("api/StandardsWhen/Details/{id}")]
        public TblStandardsWhen Details(int id)
        {
            return oWhen.Get(id);
        }

        [HttpPut]
        [Route("api/StandardsWhen/Edit")]
        public int Edit([FromBody]TblStandardsWhen standardswhen)
        {
            return oWhen.Update(standardswhen);
        }
    }
}
