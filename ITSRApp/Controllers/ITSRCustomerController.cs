using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ITSRApp.Models;

namespace ITSRApp.Controllers
{
    public class ITSRCustomerController : Controller
    {
        ITSRCustomerAccessLayer oCustomer = new ITSRCustomerAccessLayer();

        [HttpGet]
        [Route("api/Customer/Index")]
        public IEnumerable<TblCustomers> Index()
        {
            return oCustomer.GetAll();
        }

        [HttpPost]
        [Route("api/Customer/Create")]
        public int Create([FromBody] TblCustomers customer)
        {
            return oCustomer.Add(customer);
        }

        [HttpGet]
        [Route("api/Customer/Details/{id}")]
        public TblCustomers Details(int id)
        {
            return oCustomer.Get(id);
        }

        [HttpPut]
        [Route("api/Customer/Edit")]
        public int Edit([FromBody]TblCustomers customer)
        {
            return oCustomer.Update(customer);
        }
    }
}
