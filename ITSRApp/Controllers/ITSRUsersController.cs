using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ITSRApp.Models;

namespace ITSRApp.Controllers
{
    public class ITSRUsersController : Controller
    {
        ITSRUsersAccessLayer oUsers = new ITSRUsersAccessLayer();

        [HttpGet]
        [Route("api/Users/Index")]
        public IEnumerable<TblUsers> Index()
        {
            return oUsers.GetAll();
        }

        [HttpPost]
        [Route("api/Users/Create")]
        public int Create([FromBody] TblUsers user)
        {
            return oUsers.Add(user);
        }

        [HttpGet]
        [Route("api/Users/Details/{id}")]
        public TblUsers Details(int id)
        {
            return oUsers.Get(id);
        }

        [HttpPut]
        [Route("api/Users/Edit")]
        public int Edit([FromBody]TblUsers user)
        {
            return oUsers.Update(user);
        }
    }
}
