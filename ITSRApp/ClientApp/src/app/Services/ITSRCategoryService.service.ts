import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { CategoryData } from '../angularinterfaces/ITSRInterfaces';
import { Router } from '@angular/router';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';


@Injectable()
export class CategoryService {
  myAppUrl: string = "";

  constructor(private _http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.myAppUrl = baseUrl;
  }
  
  getCategoryById(id: number): Observable<CategoryData> {
    return this._http.get<CategoryData>(this.myAppUrl + "api/Category/Details/" + id)
      .catch(this.errorHandler);
  }

  //Save a new Category to the DB
  saveCategory(category: CategoryData): Observable<HttpResponse<CategoryData>> {
    let httpHeaders = new HttpHeaders()
      .set('Centent-Type', 'application/json');
    let options = {
      headers: httpHeaders
    };
    return this._http.post<CategoryData>(this.myAppUrl + 'api/Category/Create', category, options)
      .catch(this.errorHandler);
  }

  //Update a Category in the DB
  updateCategory(category: CategoryData): Observable<HttpResponse<CategoryData>> {
    let httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this._http.put(this.myAppUrl + 'api/Category/Edit', category, { headers: httpHeaders, observe: 'response' })
      .catch(this.errorHandler);
  }

  //Return an array of all categories in the DB
  getCategories(): Observable<CategoryData[]> {
    return this._http.get<CategoryData[]>(this.myAppUrl + 'api/Category/Index')
      .catch(this.errorHandler);
  }

  errorHandler(error: Response) {
    console.log(error);
    return Observable.throw(error);
  }  
}
