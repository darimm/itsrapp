export interface CategoryData {
  categoryId: number;
  category: string;
  active: boolean;
}

