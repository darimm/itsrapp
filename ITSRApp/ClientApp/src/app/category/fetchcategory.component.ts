import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { CategoryData } from '../angularinterfaces/ITSRInterfaces';

@Component({
  selector: 'app-fetch-category',
  templateUrl: './fetchcategory.component.html',
})

export class FetchCategoryComponent {
  public categoryList: CategoryData[];

  constructor(http: HttpClient, private _router: Router, @Inject('BASE_URL') baseUrl: string) {
    http.get<CategoryData[]>(baseUrl + 'api/Category/Index').subscribe(result => {
      this.categoryList = result;
    }, error => console.error(error));
  }

  edit() {
    this._router.navigate(['/register-category']);
  }

}
