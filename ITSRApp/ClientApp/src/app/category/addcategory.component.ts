import { Component, OnInit } from '@angular/core';
import { CategoryData } from '../angularinterfaces/ITSRInterfaces';
import { HttpClient } from '@angular/common/http';
import { Headers } from '@angular/http';
import { NgForm, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CategoryService } from '../Services/ITSRCategoryService.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-register-category',
  templateUrl: './AddCategory.component.html'
})

export class createcategory implements OnInit {
  dataSaved = false;
  categoryForm: FormGroup;
  title: string = "Create";
  categoryList: Observable<CategoryData[]>

  constructor(private _fb: FormBuilder, private _categoryService: CategoryService) { }
  ngOnInit() {
    this.categoryForm = this._fb.group({
      category: ['', [Validators.required]],
      active: ['', [Validators.required]]
    });
    this.saveCategory();
  }
  onFormSubmit() {
    this.dataSaved = false;
    let category = this.categoryForm.value;
    this._categoryService.getCategories().subscribe(categories => {
      this.createCategory(category);
    });
    this.categoryForm.reset();
  }

  createCategory(category: CategoryData) {
    this._categoryService.saveCategory(category).subscribe(
      category => {
        console.log(category);
        this.dataSaved = true;
        this.getAllCategories();
      },
      err => {
        console.log(err);
      }
    );
  }

  getAllCategories() {
    this.categoryList = this._categoryService.getCategories();
  }

  get category() {
    return this.categoryForm.get('category');
  }

  get active() {
    return this.categoryForm.get('active');
  }

  saveCategory() {
    this.dataSaved = false;
    let cat = this.categoryForm.value;
    this._categoryService.getCategories().subscribe(categories => {
      let maxIndex = categories.length - 1;
      let maxIndexItem = categories[maxIndex];
      this.createCategory(cat);
    });
    this.categoryForm.reset();
  }


}
